var GitHub = require( 'github' )
  , config = require( '../config.js' ).get( 'github' );

var thunkify = require( 'thunkify' );

var defaultGistParams = {
  "public": true,
  "files": {
    "cover-1.html": {
      "content": "Cover"
    },
    "cover-2.html": {
      "content": "Abstract"
    }
  }
};

module.exports = function( options ) {
  options || ( options = {} );

  var github = new GitHub({
    version: "3.0.0",
    debug: process.env.NODE_ENV !== "production" && process.env.NODE_ENV !== "prod"
  });

  github.authenticate( {
    key: config.clientID,
    secret: config.clientSecret,
    token: config.accessToken,
    type: "oauth"
  } );

  var gists = github.gists;
  var user = config.user;

  var api = {
    create: thunkify( gists.create ),
    get: thunkify( gists.get ),
    list: thunkify( gists.getFromUser ),
    update: thunkify( gists.edit )
  }

  return function *getGists ( next ) {
    var gists = yield api.list( { user: user } );
    this.body = yield gists.map( function *( gist ) {
      return yield api.get( { id: gist.id } );
    });

    if ( this.body.length === 0 ) {
      var gist = yield api.create( defaultGistParams );
      this.body = [ gist ];
    }
  }

}