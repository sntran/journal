'use strict';
var passport = require( 'koa-passport' )
  , GitHubStrategy = require( 'passport-github' ).Strategy
  , gist = require('../lib/gist')
  , conf = require( '../config' ).get( 'github')
  ;

passport.serializeUser( function ( user, done ) {
  done( null, user );
});

passport.deserializeUser( function ( user, done ) {
  done( null, user );
});

passport.use( new GitHubStrategy( {
  clientID: conf.clientID,
  clientSecret: conf.clientSecret,
  callbackURL: conf.callbackURL
}, function ( accessToken, refreshToken, profile, done ) {
  var user = (!conf.accessToken || accessToken === conf.accessToken )? profile : false;
  return done( null, user );
}));