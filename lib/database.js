"use strict";

var thunkify = require( 'thunkify' )
  , parse = require('co-body')
  , _ = require( 'lodash' )
  , models = require( '../models' )
  , Chunk = models.Chunk
  , Bucket = models.Bucket;

// Thunkify those Node.js-style callbacks into thunks.
var findBucket = thunkify( Bucket.findOne.bind( Bucket ) )
  , findBuckets = thunkify( Bucket.all.bind( Bucket ) )
  , upsertBucket = thunkify( Bucket.updateOrCreate.bind( Bucket ) )
  , findChunk = thunkify( Chunk.findOne.bind( Chunk ) )
  , findChunks = thunkify( Chunk.all.bind( Chunk ) )
  , upsertChunk = thunkify( Chunk.updateOrCreate.bind ( Chunk ) );

function getChunks( bucket ) {
  var thunk = thunkify( bucket.chunks.bind( bucket ) );
  return thunk();
}
function saveChunk( chunk ) {
  var thunk = thunkify( chunk.save.bind( chunk ) );
  return thunk();
}
function saveBucket( bucket ) {
  var thunk = thunkify( bucket.save.bind( bucket ) );
  return thunk();
}

/**
 * Cache middleware factory. Returns cache middleware which check for cached
 * data for requested bundle, or yeild for the other middleware to fulfill
 * the data and then store into cache.
 *
 * @param {Function} next
 * @return {Function}
 * @api public
 */
module.exports = function( options ) {
  options || ( options = {} );

  return function *database ( next ) {

    switch ( this.method ) {
      case 'GET':
        yield get( this, next );
        break;
      case 'POST':
        yield post( this, next );
        break;
    }
  }

  function *get( ctx, next ) {
    var refresh = ctx.query.refresh;
    try {
      var buckets = yield findBuckets( { where: { public: true } } );

      if ( buckets && buckets.length ) {
        ctx.body = yield buckets.map( function *( bucket ) {
          bucket.files = yield getChunks( bucket );
          return bucket;
        } )

        if ( !refresh ) {
          // When `refresh` query is passed, we will continue on
          // to retrieve actual data again.
          return;
        }
      }
    } catch( e ) {
      console.error( 'Fail to retrieve data from database.' );
    }

    // Wait for the next middleware to update with possibly new data.
    yield next;
    var gists = ctx.body;

    try {
      if ((ctx.method !== 'GET') || (ctx.status !== 200) || !gists) {
        return;
      }

      // Store the new data to DB and return them to body for previous consumer(s).
      ctx.body = yield gists.map( function *( gist ) {
        gist.uuid = gist.id;
        delete gist.id; // gist's id is not bucket's id.
        // Gist's files is an object with the file's name as key.
        // We can't store key with `.` to DB, so we unwrap it.
        var files = _.map( gist.files, function ( fileAttrs, filename ) {
          return fileAttrs;
        });

        var upsertedBucket = yield upsertBucket( gist );
        upsertedBucket.files = yield files.map( function*( file ) {
          var chunk = upsertedBucket.chunks.build( buildChunk( file ) );
          return yield saveChunk(chunk);
        } );

        return upsertedBucket;
      });
      
      return;
    } catch( e ) {
      console.error( 'Fail to save to database' );
    }
  }

  function *post( ctx, next ) {
    var body = yield parse( ctx );
    var bucketId = body.bucketId
        , chunkId = body.chunkId
        , content = body.body;

    if ( !bucketId || !content ) { 
      return ctx.end(); 
    }

    bucketId = parseInt( bucketId );
    var data = { 
      id: chunkId,
      content: content, 
      dirty: true,
      bucketId: bucketId,
    };

    // Upserts the data as chunk.
    var chunk = yield upsertChunk( data );
    if ( !chunk.title ) {
      chunk.title = "chunk-" + chunk.id + ".html";
      chunk = yield saveChunk( chunk );
    }

    var bucket = yield findBucket( { where: { id: bucketId } } );
    bucket.updated_at = Date.now();
    yield saveBucket( bucket );

    ctx.body = chunk;
  }
}

/**
 * Parse gist's file's attributes into Chunk data.
 *
 * @example Retrieve all gists of the authenticated user.
 * ```
 * var chunkData = buildChunk ( {
 *  type: "text/html",
 *  content: "<p>Hello World</p>"
 * } );
 * 
 * assert( chunkData.type === "text" );
 * assert( chunkData.data && chunkData.data.text === "<p>Hello World</p>" )
 * ```
 */
function buildChunk ( attrs ) {
  var chunkData = { title: attrs.filename };

  switch (attrs.type) {
    case "text/html":
    case "text/plain":
      chunkData.content = attrs.content;
      break;
    default:
      chunkData.content = attrs.content;
  }
  return chunkData;
}