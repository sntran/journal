'use strict';

var passport = require( 'passport' )
  , db = require( '../lib/database' )
  , userLib = require( '../lib/user' )()
  ;

module.exports = function spec(app) {
  app.on('middleware:after:session', function configPassport(eventargs) {
    //Give passport a way to serialize and deserialize a user. In this case, by the user's id.
    passport.serializeUser(userLib.serialize);
    passport.deserializeUser(userLib.deserialize);
    app.use(passport.initialize());
    app.use(passport.session());
  });

  return {
    onconfig: function ( config, next ) {
      config.get( 'view engines:js:renderer:arguments' ).push( app );

      db.config( config.get( 'databaseConfig' ) );

      var auth = require( '../lib/auth' )
        , gist = require( '../lib/gist' )
        , githubConf = config.get( 'github' );
      
      auth.config( githubConf );
      gist.config( githubConf );

      next(null, config);
    }
  };
};