'use strict';

// var User = require('../models/user');

var UserLibrary = function() {
  return {
    serialize: function ( user, done ) {
      done(null, user);
    },

    deserialize: function(user, done) {
      done( null, user );
    }
  }
}

module.exports = UserLibrary;