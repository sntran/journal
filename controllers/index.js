'use strict';

var parse = require('co-body');
var gist = require('../lib/gist');

var PAGEFLIP_LICENSE = process.env.PAGEFLIP || "1FgLE80ASXgZJGo0n6gnHbQHzkIUuJ6u";

module.exports.open = function *open( next ) {
  // Let other middlewares retrieve the data;
  yield next;
  var gists = this.body;
  var chunks = [];

  gists.forEach( function ( gist ) {
    gist.files.forEach( function ( file, idx ) {
      file.type = ( idx < 2 )? "cover" : "page";
      chunks.push( file );
    });

    chunks.push( { type: "page" } ); // A blank page for new entry.

    if ( chunks.length % 2 ) {
      chunks.push( { type: "page" } ); // A padding page when number of gists is odd.
    }
  });

  this.type = "text/html";
  yield this.render( 'book', { 
    bucketId: gists[0].id, 
    chunks: chunks,
    pages: chunks.length,
    editable: this.isAuthenticated && this.isAuthenticated(),
    pageflip: PAGEFLIP_LICENSE
  } );
};

module.exports.close = function *close() {
  var page = this.query.page || 0;
  this.logout();
  this.redirect( '/#/page/'+ page );
}
