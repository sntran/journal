'use strict';

var passport = require( 'koa-passport' )
  ;

module.exports.github = function *github() {
  var page = this.query.page || 0, query = "?page="+page; 

  var options = { 
    scope: 'gist',
    callbackURL: this.protocol + '://' + this.get('host') + "/auth/github/callback" + query 
  };
  yield passport.authenticate( 'github', options );
};

module.exports.callback = function *callback() {
  var page = this.query.page || 0;

  yield passport.authenticate( 'github', { 
    successRedirect: '/#/page/'+ page,
    failureRedirect: '/' 
  } );
}