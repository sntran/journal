'use strict';

module.exports = function(schema, DataTypes) {
  var BUCKET_TTL = 1000 * 60 * 60;

  var Bucket = schema.define('Bucket', {
    "id": String,
    "uuid": String,
    "description": String,
    "public": Boolean,
    "user": DataTypes.JSON,
    "created_at": Date,
    "updated_at": Date,
    "expireAt": {
      type: Date, 
      index: true, 
      default: function() {
        return new Date(new Date().getTime() + BUCKET_TTL);
      }
    }
  });

  Bucket.associate = function(models) {
    Bucket.hasMany(models.Chunk, {as: 'chunks', foreignKey: 'bucketId'});
  }
 
  return Bucket;
}