/**
Configure a connection to the database and to collect all model definitions. 
Once everything is in place, we will call the method `associate` on each of 
the models. This method will can be used to associate the model with others.
**/
var fs        = require( 'fs' )
  , path      = require( 'path' )
  , lodash    = require( 'lodash' )
  , Schema    = require( 'jugglingdb' ).Schema
  , schema
  ;
 
if (process.env.DATABASE_URL) {
  var match = process.env.DATABASE_URL.match(/([\w]+):\/\/([^:]+):([^@]+)@([^:]+):(\d+)\/(.+)/);
  var dialect = match[1], user = match[2], pass = match[3], 
      host = match[4], port = match[5], database = match[6];
  console.log( "Using", dialect, "database", database, "with user", user, "on host", host );

  schema = new Schema( dialect, {
    host: host,
    port: port,
    database: database,
    username: user,
    password: pass
  });
} else {
  console.log( "Using local database" );
  var tingodb = require( '../lib/adapters/tingodb' );
  schema = new Schema( tingodb, {
    database: './db'
  });
}
 
fs
  .readdirSync( __dirname )
  .filter( function ( file ) {
    return ( file.indexOf('.') !== 0 ) && ( file !== 'index.js' )
  })
  .forEach( function ( file ) {
    require( path.join( __dirname, file ) )( schema, Schema );
  });

var models = schema.models;
 
Object.keys( models ).forEach(function ( modelName ) {
  if ('associate' in models[modelName]) {
    models[modelName].associate( models )
  }
})
 
module.exports = models;