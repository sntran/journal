module.exports = function(schema, DataTypes) {
  var CHUNK_TTL = 1000 * 60 * 60;

  var Chunk = schema.define('Chunk', {
    "title": String,
    "content": DataTypes.Text,
    "expireAt": {
      type: Date, 
      index: true, 
      default: function() {
        return new Date(new Date().getTime() + CHUNK_TTL);
      }
    },
    "dirty": {type: Boolean, default: false}
  });

  Chunk.associate = function(models) {
    Chunk.belongsTo(models.Bucket, {as: 'bucket', foreignKey: 'bucketId'});
  }
 
  return Chunk;
}