// Why this?  Chrome has the fault:
// http://code.google.com/p/chromium/issues/detail?id=128488
function isChrome() {
  return navigator.userAgent.indexOf( 'Chrome' )!=-1;
}

var pageflipContext = $("#content").pageflip();

var $book = $( "#content" );
var pages = parseInt($book.attr("data-pages"));

pageflipContext.setPFEventCallBack({
  onFlip: function( PN ) {},
  onFlipEnd: function( PN ) {
    // When a Page finished flipping, usually two pages will fire.
  },
  onTop: function( PN ) {
  },
  onTopEnd: function( PN ) {},
  onLoad: function( PN ) {
    if ( PN === pages + 1 ) {
      $(".page-container#page"+(pages+1)).addClass("pre-last");
    }

    if ( PN === pages + 2 ) {
      $(".page-container#page"+(pages+2)).addClass("last");
    }
  },
  onUnload: function( PN ) {},
  onHide: function( PN ) {},
  onShow: function( PN ) {},
  onZoomIn: function( PN ) {},
  onZoomOut: function( PN ) {}
});

$book.pageflipInit({
  Copyright: Key.Copyright, 
  Key: Key.Key,
  CenterSinglePage: true,
  FullScreenEnabled: true,
  PageWidth: 450,
  PageHeight: 582,
  CoverWidth: 470,
  CoverHeight: 592,
  AutoScale: true,
  FullScale: true,
  FillScale: true,
  // FlexibleContent: true,
  AutoStageHeight: true,
  HotKeys: false, // Otherwise, can't type.
  Preflip: false, // Disable pre-flipping when hover Book corners
  SecondaryDragArea: 1, // Drag area width from the sides of the Book,
  DropShadowOpacity: 0.5,
  FlipTopShadowOpacity: 0.2,
  FlipShadowOpacity: 0.2,
  HardFlipOpacity: 0.3,
  EmbossOpacity: 0.2,
  PageCache: 100,
  HashControl: true
});

$(function() {
  $('#pen').click(function() {
    var pageNumber = $book.pageflip().getPageNumber().h;
    var bucket = $book.attr( "data-bucket" );
    this.href = this.href + "?page=" + pageNumber + "&bucket=" + bucket;
    return true;
  });
});

// function UserStyles() {
//   this.background = 'Melamine-wood-004';
//   this.cover = "webtreats-grungy-teal1";
//   this.paper = "Texture-98-by-S3PTIC-STOCK";
// }

// window.onload = function() {
//   var gui = new dat.GUI();
//   var userStyles = new UserStyles();

//   gui.remember(userStyles);

//   gui.add( userStyles, 'background',  [ 'Melamine-wood-001', 'Melamine-wood-002', 'Melamine-wood-003', 'Melamine-wood-004', 'Melamine-wood-005', 'Melamine-wood-006' ] )
//   .onChange( function ( value ) {
//     $( "body" ).css( 'background', "url('/img/wood/PNGs/"+value+".png')" );
//     gui.save();
//   });

//   gui.add( userStyles, 'cover',  [ 'webtreats-grungy-teal1', 'webtreats-grungy-teal2', 'webtreats-grungy-teal3', 'webtreats-grungy-teal4', 'webtreats-grungy-teal5', 'webtreats-grungy-teal6' ] )
//   .onChange( function ( value ) {
//     $( ".book-content #page1, .book-content #page2, .book-content .pre-last, .book-content last" ).css( 'background', "url('/img/cover/"+value+".jpg')" );
//     gui.save();
//   });

//   gui.add( userStyles, 'paper', [ 'Texture-2-by-S3PTIC-STOCK', 'Texture-3-by-S3PTIC-STOCK', 'Texture-77-by-S3PTIC-STOCK', 'Texture-86-by-S3PTIC-STOCK', 'Texture-89-by-S3PTIC-STOCK', 'Texture-91-by-S3PTIC-STOCK', 'Texture-92-by-S3PTIC-STOCK', 'Texture-97-by-S3PTIC-STOCK', 'Texture-98-by-S3PTIC-STOCK' ] )
//   .onChange( function ( value ) {
//     $( ".book-content .page-container:not(#page1, #page2, .last, .pre-last)" ).css( 'background', "url('/img/paper/"+value+".jpg')" );
//     gui.save();
//   });
  
//   gui.revert();
// };
