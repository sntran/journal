jQuery(window).load(function() {
  jQuery( '.chunk' ).each(function() {
    var $this = jQuery( this ),
        id = $this.attr( "id" ),
        bucketId = $book.attr( "data-bucket" );
    // $this.editable({
    //   alwaysVisible: true,
    //   spellcheck: true,
    //   autosave: true,
    //   autosaveInterval: 2000,
    //   saveURL: "/",
    //   saveParams: { bucketId: bucketId, chunkId: id},
    //   afterSaveCallback: function ( data ) {
    //     $this.attr( "id", data.id );
    //     $this.editable( "option", "saveParams", {
    //       bucketId: bucketId,
    //       chunkId: data.id
    //     });
    //   }
    // });
  });

  var editor = new MediumEditor( '.chunk', {
    buttonLabels: 'fontawesome',
    firstHeader: 'h1',
    secondHeader: 'h2'
  });

  var $chunks = jQuery( '.chunk' );

  $chunks.on( 'input', function() {
    save( editor );
  });

  $chunks.mediumInsert( {
    editor: editor,
    addons: {
      images: {
        imagesUploadScript: '', // We don't need to upload file.
        imagesDeleteScript: '', // We don't need to delete file.
        uploadFile: function ( $placeholder, file, that ) {
          var maxWidth = $placeholder.innerWidth();
          // Mark the completion of "uploading"
          var $progress = jQuery( '.progress:first', $placeholder );
          $progress.attr( 'value', 100 );
          $progress.html( 100 );

          // Create a canvas element and insert into the $placeholder
          var canvasEl = document.createElement( 'canvas' );
          var $editBtn = jQuery( '<i class="fa fa-pencil" style="position: absolute; left: 1em; cursor: pointer; z-index: 10;"></i>' );
          var $deleteBtn = jQuery( '<i class="fa fa-times" style="position: absolute; left: 0; cursor: pointer; z-index: 10;"></i>' );
          $placeholder.append( canvasEl );

          // Initialize a Fabric canvas and load the image object in.
          var canvas = new fabric.Canvas( canvasEl );
          canvas.setWidth( maxWidth );
          var freeDrawingBrush = canvas.freeDrawingBrush;
          freeDrawingBrush.color = $placeholder.css( 'color' );
          freeDrawingBrush.width = parseInt( $placeholder.css( 'font-size') );

          function onCanvasChanged() {
            setCanvasHeight( canvas );
            save( editor );
          }

          // When the canvas has changed, we calculate the max height
          // and set the canvas to it so we can see everything.
          canvas.on( 'object:modified', onCanvasChanged );
          canvas.on( 'object:added', onCanvasChanged );
          canvas.on( 'object:removed', onCanvasChanged );

          var reader = new FileReader();
          reader.onload = function ( event ){
            addImage( canvas, event.target.result );
          }
          reader.readAsDataURL( file );

          $placeholder.hover( function() {
            // Toggle extra buttons for the canvas.
            $placeholder.prepend( $editBtn ).prepend( $deleteBtn );

            var json = jQuery( 'img', $placeholder ).remove().attr( 'data-json' );
            if ( !json ) return;
            // Use the JSON in data- attriute to reconstruct the canvas.
            canvas.loadFromJSON( decodeURIComponent( json ) );
          }, function() {
            jQuery( '.fa-pencil, .fa-times', $placeholder ).remove();

            if ( !canvas.getObjects().length ) return;
            // Render the cavas as an image, and store its JSON in data- attribute.
            var dataURL = canvas.toDataURL(), json = JSON.stringify( canvas );
            canvas.clear();
            $placeholder.prepend( '<img src="' + dataURL + '" data-json="'+encodeURIComponent( json )+'" />' );
          } );

          $placeholder.on( 'click', '.fa-times', function() {
            // Either remove selected object, or the whole canvas.
            var activeObject = canvas.getActiveObject();
            activeObject && canvas.remove( activeObject );
            if ( canvas.getObjects().length === 0) { 
              canvas.dispose();
              $placeholder.empty();
              jQuery.fn.mediumInsert.insert.deselect();
            }
          } );

          $placeholder.on( 'click', '.fa-pencil', function() {
            // Toggle free drawing mode.
            var isDrawingMode = canvas.isDrawingMode;
            canvas.isDrawingMode = !isDrawingMode;
            // Change icon
            jQuery( this ).toggleClass( 'fa-crop', !isDrawingMode );
          } );

          $progress.remove();
          $.fn.mediumInsert.insert.$el.keyup();
        }
      },
      embeds: {}
    }
  });
});

function addImage(canvas, src) {
  var maxWidth = canvas.getWidth();
  fabric.Image.fromURL( src, function ( oImg ) {
    oImg.perPixelTargetFind = true;

    if ( oImg.get( 'width') > maxWidth ) {
      oImg.scaleToWidth( maxWidth );
    }

    canvas.add( oImg );
  } );
}

function setCanvasHeight( canvas ) {
  var heights = canvas.getObjects().map( function ( obj ) {
    var aabb = obj.getBoundingRect();
    return aabb.top + aabb.height;
  } );
  var maxHeight = Math.max.apply( Math, heights );
  canvas.setHeight( maxHeight );
}

function save( editor, canvas ) {
  var pages = editor.elements;

  // @TODO: Remove canvases
  // @TODO: Detect current page
  // @TODO: POST to server.
}
