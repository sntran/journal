'use strict';

var book = require( './controllers/index' );
var auth = require( './controllers/auth' );
var config = require( './config');

var database = require( './lib/database' )();
var gist = require( './lib/gist' )();

var compress = require( 'koa-compress' );
var logger = require( 'koa-logger' );
var serve = require( 'koa-static-cache' );
var router = require('koa-router');
var views = require( 'koa-views' );
var stylus = require('koa-stylus');
var session = require( 'koa-session' );
var passport = require( 'koa-passport' );

var koa = require( 'koa' );
var path = require( 'path' );
var app = module.exports = koa();

require( './lib/auth' );
app.keys = ['SonUyenBinhBa'];
app.use( session() );
app.use( passport.initialize() );
app.use( passport.session() );

// Logger
app.use( logger() );
app.use( views( __dirname + '/public/templates', {
  default: 'dust',
  map: { dust: 'dust' }
} ) );
// Compress response.
app.use( compress() );
// Set up routing.
app.use( router(app) );

app.get( '/', book.open, database, gist );
app.post( '/', database );

app.get( '/logout', book.close );

app.get( '/auth/github', auth.github );
app.get( '/auth/github/callback', auth.callback );

// Convert stylus to css
app.use( stylus( {
  src: './public',
  dest: './public'
} ) );
// Serve static files
app.use( serve( 'public', { 
  maxAge: 365 * 24 * 60 * 60,
  gzip: true
} ) );

if (!module.parent) {
  var port = config.get( "PORT" );
  app.listen( port );
  console.log( 'listening on port', port );
}